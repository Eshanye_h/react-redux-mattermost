import React, { Component } from "react";
import { Input } from "antd";
import UserDetails from "./UserContext";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
// @ts-ignore
import { createPost } from "mattermost-redux/actions/posts";

interface istate {
  inputtext: string;
}
interface iprops {
  createPost: (post: any, ...args: any[]) => any;
  selectedChannelId: string;
  updatepost: () => Promise<void>;
}

class Inputfield extends Component<iprops, istate> {
  state = {
    inputtext: ""
  };
  inputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      inputtext: e.target.value
    });
  };
  keyPress = (event: React.KeyboardEvent, userId: string, userName: string) => {
    console.log(userId);
    if (event.key === "Enter") {
      this.addNewMessage(
        this.state.inputtext,
        userId,
        _.get(this.props, "selectedChannelId", ""),
        userName
      );
      this.setState({
        inputtext: ""
      });
    }
  };
  addNewMessage = async (
    input: string,
    userid: string,
    channelid: string,
    username: string
  ) => {
    const postMessage = await this.props.createPost({
      message: input,
      user_id: userid,
      channel_id: channelid,
      props: {
        username: username
      }
    });
    this.props.updatepost();
    console.log(postMessage);
  };
  render() {
    return (
      <div>
        <UserDetails.Consumer>
          {value => {
            return (
              <Input
                style={{
                  // position: "absolute",
                  marginTop: "30px",
                  //bottom: "10px",
                  right: "30px",
                  width: "1200px",
                  height: "50px"
                }}
                type="text"
                value={this.state.inputtext}
                onChange={this.inputChange}
                onKeyPress={e => this.keyPress(e, value.userId, value.userName)}
              />
            );
          }}
        </UserDetails.Consumer>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      createPost
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(Inputfield);
