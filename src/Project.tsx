import React, { Component } from "react";
import ContentMessage from "./contentMessage";
import { Layout, Breadcrumb, Button, Icon } from "antd";
import SliderContent from "./sliderContent";
import shallowCompare from "react-addons-shallow-compare";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// @ts-ignore
import { logout } from "mattermost-redux/actions/users";
// @ts-ignore
import { getPosts } from "mattermost-redux/actions/posts";
//@ts-ignore
import { Channel } from "mattermost-redux/types/channels";

const { Header, Content, Sider } = Layout;
interface Istate {
  selectedChannel: string;
  selectedChannelNAme: string;
}
// interface indexSignature {
//   [k: string]: Channel[];
// }

interface IProps {
  channels: Channel;
  teamid: string;
  logout: () => any;
  logoutUser: (value: boolean) => void;
}
class Project extends Component<IProps, Istate> {
  state = {
    selectedChannel: "",
    selectedChannelNAme: ""
  };
  //Handle onChange of channels
  selectChanneldetails = (id: string, name: string) => {
    this.setState({
      selectedChannel: id,
      selectedChannelNAme: name
    });
  };
  //Logout users
  logoutUser = async () => {
    const { data } = await this.props.logout();
    if (data) {
      this.props.logoutUser(false);
    } else {
      alert("error in logout");
    }
  };
  shouldComponentUpdate(nextProps: IProps, nextState: Istate): boolean {
    return shallowCompare(this, nextProps, nextState);
  }
  render() {
    console.log("rendering Project");
    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider collapsible>
          <div className="logo" />
          <h1 className="appName" style={{ color: "white", fontSize: "40px" }}>
            Chit Chat
          </h1>
          <SliderContent
            selectChanneldetails={this.selectChanneldetails}
            teamid={this.props.teamid}
          />
        </Sider>
        <Layout>
          <Header style={{ background: "rgba(0, 0, 0)", padding: 0 }}>
            <div
              style={{
                color: "white",
                fontFamily: "monospace",
                fontStyle: "semibold",
                fontSize: "30px",
                padding: "10px"
              }}
            >
              {this.state.selectedChannelNAme}
              <Button
                ghost
                style={{ margin: "10px", float: "right" }}
                onClick={this.logoutUser}
              >
                <Icon type="poweroff" />
                Logout
              </Button>
              <Button type="danger" style={{ margin: "10px", float: "right" }}>
                Add User
              </Button>
              <Layout />
            </div>
          </Header>
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Channel</Breadcrumb.Item>
              <Breadcrumb.Item>
                {this.state.selectedChannelNAme}
              </Breadcrumb.Item>
            </Breadcrumb>
            <div
              style={{
                padding: 24,
                background: "#fff",
                minHeight: 360
              }}
            >
              <ContentMessage selectedChannelId={this.state.selectedChannel} />
            </div>
          </Content>
        </Layout>
      </Layout>
    );
  }
}
const mapStateToProps = (state: any) => {
  //console.log("State: ", state);
  return {
    // entity: state.entities
    channels: state.entities.channels.channels
  };
};
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      logout,
      getPosts
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Project);
// export default
