import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button, Icon, Input } from "antd";
import shallowCompare from "react-addons-shallow-compare";
// @ts-ignore
import { login } from "mattermost-redux/actions/users";
// @ts-ignore
import { UsersState } from "mattermost-redux/types/users";
import _ from "lodash";
interface LoginProps {
  login: (username: string, password: string) => any;
  users: UsersState;
  updateState: (
    value: boolean,
    teamid: string,
    userid: string,
    username: string
  ) => void;
  teamid: string;
}
interface istate {
  username: string;
  password: string;
}
class Login extends Component<LoginProps, istate> {
  state = {
    username: "",
    password: ""
  };

  handleOnUsername = (e: React.ChangeEvent<HTMLInputElement>) => {
    let username: string = e.target.value;
    this.setState({
      username: username
    });
  };

  handleOnPassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      password: e.target.value
    });
  };

  login = async () => {
    const user = await this.props.login(
      this.state.username.toLocaleLowerCase(),
      this.state.password
    );
    console.log("after login ", user);
    if (user.data) {
      this.props.updateState(
        true,
        _.get(_.keys(this.props.teamid), "0", ""),
        this.props.users.currentUserId,
        this.props.users.profiles[this.props.users.currentUserId].username
      );
    } else {
      alert("invalid username or password");
    }
    this.setState({
      username: "",
      password: ""
    });
  };
  shouldComponentUpdate(nextProps: LoginProps, nextState: istate): boolean {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    console.log("rendering login");
    //console.log("User from props: ", this.props.users);
    return (
      <div
        style={{
          width: "600px",
          margin: "auto",
          marginTop: "100px",
          padding: "80px",
          boxShadow: "5px 5px 5px #d2d2d2"
        }}
      >
        <div className="logo">
          <h1
            style={{
              color: "black",
              fontFamily: "serif",
              fontStyle: "italic",
              fontSize: "50px"
            }}
          >
            ChitChat
          </h1>
        </div>
        <p>Login</p>
        <Input
          size="large"
          prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
          placeholder="Username"
          onChange={this.handleOnUsername}
          value={this.state.username}
        />
        <br />
        <br />
        <Input
          size="large"
          prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
          type="password"
          placeholder="Password"
          onChange={this.handleOnPassword}
          value={this.state.password}
        />
        <br />
        <br />
        <Button
          size="large"
          type="primary"
          onClick={this.login}
          htmlType="submit"
          className="login-form-button"
        >
          Log in
        </Button>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  //console.log("State: ", state);
  return {
    // entity: state.entities
    users: _.get(state, "entities.users", {}),
    teamid: _.get(state, "entities.teams.teams", {})
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      login
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
