import React, { Component } from "react";
import { Menu, Icon, Button } from "antd";
import { ClickParam } from "antd/lib/menu";
import shallowCompare from "react-addons-shallow-compare";
//@ts-ignore
import { Channel } from "mattermost-redux/types/channels";
// @ts-ignore
import { getChannels, selectChannel } from "mattermost-redux/actions/channels";
import ModalClass from "./modal";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "lodash";
interface IProps {
  selectChanneldetails: (id: string, name: string) => void;
  getChannels: (teamId: any, ...args: any[]) => any;
  selectChannel: (
    channelId: any
  ) => {
    type: any;
  };
  channels: Channel;
  teamid: string;
}
interface index {
  [k: string]: Channel;
}
interface istate {
  channelList: Channel;
  selectedChannel: string;
  selectedChannelNAme: string;
  loading: boolean;
  teamid: string;
}
class SliderContent extends Component<IProps, istate> {
  state = {
    channelList: {},
    selectedChannel: "",
    loading: true,
    teamid: "",
    selectedChannelNAme: ""
  };
  getallchannels = async () => {
    console.log("teamid", this.props.teamid);
    // const tid = _.head(_.keys(this.props.teamid)) as string;
    // console.log("teamidfinal", tid);
    //console.log(this.props.channels);
    let tid = this.props.teamid;
    await this.props.getChannels(tid);
    console.log("channelName");
    console.log(this.props.channels);
    const selectedChannel = _.get(_.keys(this.props.channels), "0", "");
    console.log("channelName");
    console.log(selectedChannel);
    const selectedChannelNAme = _.get(this.props.channels, [
      selectChannel,
      "display_name"
    ]); //this.props.channels[selectedChannel]["display_name"];
    this.setState(
      {
        channelList: this.props.channels,
        selectedChannel: selectedChannel,
        selectedChannelNAme: selectedChannelNAme,
        loading: false
      },
      () => {
        this.selectChannel();
        this.props.selectChanneldetails(
          this.state.selectedChannel,
          this.state.selectedChannelNAme
        );
      }
    );
  };

  //function to have a selected channel even after refresh
  selectChannel = async () => {
    await this.props.selectChannel(this.state.selectedChannel);
  };

  menuChange = ({ key }: ClickParam) => {
    const selectedChannelNAme = _.get(this.props.channels, [key, "name"], "");
    this.setState(
      {
        selectedChannel: key,
        selectedChannelNAme: selectedChannelNAme
      },
      () => {
        this.props.selectChanneldetails(
          this.state.selectedChannel,
          this.state.selectedChannelNAme
        );
      }
    );
  };

  // static getDerivedStateFromProps(props: IProps, state: istate) {
  //   if (props.channelList !== state.channelList) {
  //     const defaultChannel = _.head(_.keys(props.channelList));
  //     console.log(defaultChannel);
  //     if (defaultChannel) {
  //       return {
  //         channelList: props.channelList,
  //         selectedChannel: defaultChannel,
  //         loading: false
  //       };
  //     }
  //   }
  //   return state;
  // }
  componentDidUpdate(prevProps: IProps, prevState: istate) {
    if (prevProps.teamid !== this.props.teamid) {
      this.setState(
        {
          teamid: this.props.teamid
        },
        () => {
          this.getallchannels();
        }
      );
    }
  }

  componentDidMount() {
    this.setState(
      {
        teamid: this.props.teamid
      },
      () => {
        this.getallchannels();
      }
    );
  }
  shouldComponentUpdate(nextProps: IProps, nextState: istate): boolean {
    return shallowCompare(this, nextProps, nextState);
  }
  render() {
    console.log("rendering sidebar");
    // if (Object.keys(this.props.channelList).length === 0) {
    const channellist: index = this.state.channelList;

    return (
      <React.Fragment>
        {this.state.loading ? (
          <Button type="primary" loading>
            Loading
          </Button>
        ) : (
          <Menu
            theme="dark"
            defaultSelectedKeys={[this.state.selectedChannel]}
            onClick={this.menuChange}
            mode="inline"
          >
            {_.size(_.keys(channellist)) > 0 ? (
              _.keys(channellist).map(eachChannelKey => (
                <Menu.Item key={eachChannelKey}>
                  <Icon type="aliwangwang" />

                  <span>{channellist[eachChannelKey]["display_name"]}</span>
                </Menu.Item>
              ))
            ) : (
              <div>no channel to display</div>
            )}
          </Menu>
        )}
        <ModalClass teamid={this.state.teamid} />
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state: any) => {
  //console.log("State: ", state);
  return {
    // entity: state.entities
    channels: state.entities.channels.channels
  };
};
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getChannels,
      selectChannel
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SliderContent);
