import React, { Component } from "react";
import { List, Avatar } from "antd";
import shallowCompare from "react-addons-shallow-compare";
import _ from "lodash";
// @ts-ignore
import { getChannelMembers } from "mattermost-redux/actions/channels";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
//@ts-ignore
import { UsersState } from "mattermost-redux/types/users";
// @ts-ignore
import { getProfilesByIds } from "mattermost-redux/actions/users";

interface Iprops {
  selectedChannelId: string;
  getChannelMembers: (channelId: any, ...args: any[]) => any;
  getProfilesByIds: (userIds: any, options: any) => any;
}
interface Istate {
  selectedChannelId: string;
  members: string[];
}

class ChannelMembers extends Component<Iprops, Istate> {
  state = {
    selectedChannelId: "",
    members: []
  };
  // static getDerivedStateFromProps(props: Iprops, state: Istate) {
  //   if (props.selectedChannelId !== state.selectedChannelId) {
  //     return {
  //       selectedChannelId: props.selectedChannelId
  //     };
  //   }
  //   return state;
  // }
  componentDidUpdate(prevProps: Iprops, prevState: Istate) {
    if (prevProps.selectedChannelId !== prevState.selectedChannelId) {
      console.log("inside if of componentDidUpdate");
      this.setState(
        {
          selectedChannelId: prevProps.selectedChannelId
        },
        () => {
          this.getchannelmembers();
        }
      );
    }
  }

  getchannelmembers = async () => {
    console.log("function call");
    console.log("channelid", this.state.selectedChannelId);
    let channelMemberDetails = await this.props.getChannelMembers(
      this.state.selectedChannelId
    );
    let data = _.get(channelMemberDetails, "data", []);
    const userid = data.map((elem: UsersState) => elem.user_id);
    let memberdetails = await this.props.getProfilesByIds(userid, {});
    const memberdata = _.get(memberdetails, "data", []);
    const members = memberdata.map((elem: UsersState) => elem.username);
    console.log(members);
    this.setState({
      members: members
    });
  };
  shouldComponentUpdate(nextProps: Iprops, nextState: Istate): boolean {
    return shallowCompare(this, nextProps, nextState);
  }
  componentDidMount() {
    this.setState(
      {
        selectedChannelId: _.get(this.props, "selectedChannelId", "")
      },
      () => {
        this.getchannelmembers();
      }
    );
  }

  render() {
    console.log("rendering channel members");
    console.log(this.state.members);
    return (
      <div
        style={{
          background: "white",
          border: "1px solid",
          overflow: "scroll",
          height: "550px"
        }}
      >
        <List
          header={
            <div
              style={{
                color: "black",
                fontWeight: 50
              }}
            >
              Channel Members
            </div>
          }
          bordered
          dataSource={this.state.members}
          renderItem={eachmember => (
            <List.Item>
              <Avatar
                style={{
                  color: "#f56a00",
                  backgroundColor: "#fde3cf",
                  float: "left",
                  margin: "10px"
                }}
                size="small"
              >
                {eachmember[0]}
              </Avatar>
              <p>{eachmember}</p>
            </List.Item>
          )}
        />
      </div>
    );
  }
}

// const mapStateToProps = (state: any) => {
//   //console.log("State: ", state);
//   return {
//     channels: state.entities.channels.channels
//   };
// };
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getChannelMembers,
      getProfilesByIds
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(ChannelMembers);
