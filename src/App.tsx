import React from "react";
import "./App.css";
import "antd/dist/antd.css";
import Project from "./Project";
import UserDetails from "./UserContext";
import Login from "./Login";
import { Provider } from "react-redux";
// @ts-ignore
import configureServiceStore from "mattermost-redux/store";
import _ from "lodash";
// @ts-ignore
import { Client4 } from "mattermost-redux/client";

const offlineOptions = {
  persistOptions: {
    autoRehydrate: {
      log: true
    }
  }
};

interface istate {
  login: boolean;
  teamid: string;
  userid: string;
  username: string;
}
const store = configureServiceStore({}, {}, offlineOptions);
Client4.setUrl("https://communication.hotelsoft.tech");
class App extends React.Component<any, istate> {
  state = { login: false, teamid: "", userid: "", username: "" };
  updateState = (
    value: boolean = false,
    team: string = "",
    userid: string = "",
    username: string = ""
  ) => {
    //const teamid = _.head(_.keys(team));
    if (value && team && userid && username) {
      this.setState({
        login: value,
        teamid: team,
        userid: userid,
        username: username
      });
    }
  };
  logout = () => {
    this.setState((prevState: any) => ({
      login: !prevState.login
    }));
  };
  render() {
    console.log("rendering app");
    return (
      <UserDetails.Provider
        value={{
          userId: this.state.userid,
          userName: this.state.username,
          login: this.state.login
        }}
      >
        <Provider store={store}>
          <div>
            {!this.state.login ? (
              <Login updateState={this.updateState} />
            ) : (
              <div className="App">
                <Project teamid={this.state.teamid} logoutUser={this.logout} />
              </div>
            )}
          </div>
          )}
          {/**/}
        </Provider>
      </UserDetails.Provider>
    );
  }
}

export default App;
