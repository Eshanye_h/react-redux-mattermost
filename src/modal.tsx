import React, { Component } from "react";
import { Modal, Input, Button, Icon } from "antd";
import UserDetails from "./UserContext";
// @ts-ignore
import { createChannel } from "mattermost-redux/actions/channels";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
interface IProps {
  createChannel: (channel: any, userId: any) => any;
  teamid: string;
}
class ModalClass extends Component<IProps> {
  state = { newChannel: "", visible: false };
  handlenewChannel = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newChannel: e.target.value
    });
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  updateNewChannel = async (name: string, userid: string) => {
    //console.log(this.props.teamid);
    // const tid = _.head(_.keys(this.props.teamid));
    const tid = this.props.teamid;
    let displayname = name.toLocaleLowerCase();
    displayname = displayname.replace(/-/g, "");
    let newchannel = await this.props.createChannel(
      { display_name: displayname, name: name, team_id: tid, type: "O" },
      userid
    );
    console.log(newchannel);
  };
  handleOk = (userid: string) => {
    this.updateNewChannel(this.state.newChannel, userid);
    this.setState({
      visible: false,
      newChannel: ""
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
      newChannel: ""
    });
  };
  render() {
    return (
      <React.Fragment>
        <Button.Group size="large">
          <Button type="primary" onClick={this.showModal}>
            <Icon type="plus-circle" />
            Add new Channel
          </Button>
        </Button.Group>
        <UserDetails.Consumer>
          {value => {
            return (
              <Modal
                title="Add New Channel"
                visible={this.state.visible}
                onOk={() => {
                  this.handleOk(value.userId);
                }}
                onCancel={this.handleCancel}
              >
                <label>Enter name of new channel</label>
                <Input
                  type="text"
                  value={this.state.newChannel}
                  onChange={this.handlenewChannel}
                />
              </Modal>
            );
          }}
        </UserDetails.Consumer>
      </React.Fragment>
    );
  }
}
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      createChannel
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(ModalClass);
