import React, { Component } from "react";
import { Card, Avatar } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Inputfield from "./Inputfield";
import ChannelMembers from "./ChannelMembers";
import shallowCompare from "react-addons-shallow-compare";
// @ts-ignore
import { getPosts } from "mattermost-redux/actions/posts";
//@ts-ignore
import { Post } from "mattermost-redux/types/posts";
import _ from "lodash";

interface Istate {
  channelPost: Post[];
  selectedChannelId: string;
}

interface IProps {
  selectedChannelId: string;
  getPosts: (channelId: any, ...args: any[]) => any;
}

class ContentMessage extends Component<IProps, Istate> {
  state: Istate = {
    selectedChannelId: "",
    channelPost: []
  };
  getallpost = async () => {
    //console.log(this.state.selectedChannel);
    const postdata: Post = await this.props.getPosts(
      this.state.selectedChannelId
    );
    console.log(postdata);

    const orderby = _.get(postdata.data, "order", []);
    const dataTosend = orderby
      .slice(0)
      .reverse()
      .map((MessageKey: string) =>
        _.get(postdata.data, ["posts", MessageKey], "")
      );
    this.setState({
      channelPost: dataTosend
    });
    console.log("updated post");
    console.log(this.state.channelPost);
  };
  // static getDerivedStateFromProps(props: IProps, state: Istate) {
  //   if (props.selectedChannelId !== state.selectedChannelId) {
  //     return {
  //       selectedChannelId: props.selectedChannelId
  //     };
  //   }
  //   return state;
  // }
  componentDidUpdate(props: IProps, state: Istate) {
    if (props.selectedChannelId !== state.selectedChannelId) {
      this.setState(
        {
          selectedChannelId: this.props.selectedChannelId
        },
        () => {
          this.getallpost();
        }
      );
    }
  }
  // shouldComponentUpdate(nextProps: IProps, nextState: Istate): boolean {
  //   return shallowCompare(this, nextProps, nextState);
  // }
  componentDidMount() {
    this.setState(
      {
        selectedChannelId: this.props.selectedChannelId
      },
      () => {
        this.getallpost();
      }
    );
  }

  render() {
    console.log("rendering contextmessage");
    console.log(this.state.channelPost);
    const channeldetails: any = this.state.channelPost;

    return (
      <div>
        <div
          style={{
            overflow: "scroll",
            height: "550px",
            width: "105vh",
            float: "left",
            marginRight: "50px"
          }}
        >
          {_.size(_.keys(channeldetails)) > 0 ? (
            _.keys(channeldetails).map((eachMessageid, index) => (
              <div
                key={index}
                style={{
                  background: "#ECECEC",
                  padding: "10px"
                }}
              >
                <Card
                  size="small"
                  title={channeldetails[eachMessageid].props.username}
                  bordered={false}
                  style={{ width: "100vh" }}
                >
                  <Avatar
                    style={{
                      color: "#f56a00",
                      backgroundColor: "#fde3cf",
                      float: "left",
                      margin: "5px"
                    }}
                    size="large"
                  >
                    {channeldetails[eachMessageid].props.username}
                  </Avatar>
                  <p style={{ float: "left" }}>
                    {channeldetails[eachMessageid]["message"]}
                  </p>
                </Card>
              </div>
            ))
          ) : (
            <div>No post to display.....</div>
          )}
        </div>
        <div style={{ width: "30vh", float: "left" }}>
          <ChannelMembers selectedChannelId={this.state.selectedChannelId} />
        </div>
        <Inputfield
          selectedChannelId={this.state.selectedChannelId}
          updatepost={this.getallpost}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPosts
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(ContentMessage);
